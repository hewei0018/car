package com.controller;

import com.service.UserService;
import com.util.Result;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * 用户信息管理与登录
 * 登录页面和用户信息管理
 */
@Controller
@RequestMapping("/user")
public class UserController {
	@Resource
	private UserService us;

	//登录到系统，传账户和密码到业务层进行判断用户类型
	@RequestMapping(value = "/login.do")
	@ResponseBody
	public Result execute(String name,String password){
		Result rs=us.login(name, password);
		System.out.println("登录用户："+name);
		System.out.println(rs.toString());
		return rs;
	}

	//用户中，修改密码功能
	@RequestMapping("/changepwd.do")
	@ResponseBody
	public Result changePwd(String user_id, String mpass, String newpass) {
		Result rs=us.changePwd(user_id, mpass, newpass);
		System.out.println(rs);
		return rs;
	}


	//后台中，查询所有的用户，返回列表
	@RequestMapping("/show.do")
	@ResponseBody
	public Result userList() {
		Result rs=us.userList();
		System.out.println(rs);
		return rs;
	}

	//后台中，添加一个用户
	@RequestMapping("/add.do")
	@ResponseBody
	public Result addUser(String name, String password, Integer type){
		Result rs=us.addUser(name, password, type);
		System.out.println(rs);
		return rs;
	}

	//根据传入的id，改变用户的注册状态
	@RequestMapping("/change.do")
	@ResponseBody
	public Result changeStatus(String id){
		Result rs=us.changeStatus(id);
		System.out.println(rs);
		return rs;
	}
}
