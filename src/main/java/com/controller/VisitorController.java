package com.controller;
/**
 * 收集管理员用户登录信息
 */
import com.service.VisitorService;
import com.util.Result;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
@Controller
@RequestMapping("/visitor")
public class VisitorController {
	@Resource
	private VisitorService vs;

	//查询所有的登录历史
	@RequestMapping("/show.do")
	@ResponseBody
	public Result visitorList() {
		System.out.println("进入VisitorController/show.do");
		Result rs=vs.visitorList();
		System.out.println(rs);
		return rs;	
	}
}
