package com.controller;

import com.service.PersonalInfoService;
import com.util.Result;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * 个人信息管理
 * 展示和修改个人信息
 */
@Controller
@RequestMapping("/personalinfo")
public class PersonalInfoController {
	@Resource
	private PersonalInfoService ps;

	//修改个人信息，用户创建后，个人信息默认为空，需要修改来添加
	@RequestMapping("/update.do")
	@ResponseBody
	public Result execute(String user_id,String real_name,String sex,String birthday,
			String email,String address,String tel_num,String remark){
		System.out.println("修改个人信息");
		Result rs=ps.updatePersonalInfo(user_id, real_name, sex, birthday, email, address, tel_num, remark);
		System.out.println(rs.toString());
		return rs;
	}

	//展示个人信息
	@RequestMapping("/show.do")
	@ResponseBody
	public Result execute(String user_id){
		System.out.println("根据传入的用户id，查询所有的用户信息");
		Result rs=ps.showPersonalInfo(user_id);
		System.out.println(rs.toString());
		return rs;
	}
}
