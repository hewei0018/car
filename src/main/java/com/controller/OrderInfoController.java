package com.controller;

import com.service.OrderInfoService;
import com.util.Result;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * 维修信息管理
 * 后台与用户所有的维修订单的处理
 */
@Controller
@RequestMapping("/orderinfo")
public class OrderInfoController {
	@Resource
	private OrderInfoService os;

	//添加维修订单，根据输入的信息，调用dao完成添加
	@RequestMapping("/add.do")
	@ResponseBody
	public Result addOrderInfo(String user_id, String user_name, String plate, String trouble_code,String trouble_name, String contact,
			String contact_way, String remark) {
		System.out.println("添加故障信息：");
		Result rs=os.addOrderInfo(user_id, user_name, plate, trouble_code,trouble_name, contact, contact_way, remark);
		System.out.println(rs);
		return rs;		
	}

	//调用方法，获取所有的维修订单
	@RequestMapping("/showAll.do")
	@ResponseBody
	public Result findAllOrder() {
		System.out.println("展示所有的维修订单信息");
		Result rs=os.findAllOrder();
		System.out.println(rs);
		return rs;	
	}

	//传入一个id 根据传入的id值 删除一个维修订单
	@RequestMapping("/delete.do")
	@ResponseBody
	public Result delOrderInfo(String id) {
		System.out.println("根据id删除一个维修订单信息");
		Result rs=os.delOrderInfo(id);
		System.out.println(rs);
		return rs;
	}

	//传入一个id值，把车辆的维修信息改为已处理
	@RequestMapping("/change.do")
	@ResponseBody
	public Result changStatus(String id) {
		System.out.println("传入id，调整维修状态为已处理");
		Result rs=os.changStatus(id);
		System.out.println(rs);
		return rs;
	}

	//通过用户id进行模糊查询
	@RequestMapping("/show.do")
	@ResponseBody
	public Result findOrder(String user_id) {
		System.out.println("通过传入的用户id 查询维修信息");
		Result rs=os.findOrder(user_id);
		System.out.println(rs);
		return rs;
	}

    //调用后，按时间排序查询结果
	@RequestMapping("/sort.do")
	@ResponseBody
	public Result findBySort() {
		System.out.println("按时间排序，展示查询结果");
		Result rs=os.findBySort();
		System.out.println(rs);
		return rs;
	}

    //调用后，按照已处理状态查询并返回结果
	@RequestMapping("/deal.do")
	@ResponseBody
	public Result findDealOrder(){
		System.out.println("按已处理状态查询维修信息");
		Result rs=os.findDealOrder();
		System.out.println(rs);
		return rs;
	}

	//调用后，按照未处理状态查询并返回结果
	@RequestMapping("/undeal.do")
	@ResponseBody
	public Result findUndealOrder(){
		System.out.println("按未处理状态查询维修信息");
		Result rs=os.findUndealOrder();
		System.out.println(rs);
		return rs;
	}

	//输入关键字进行搜索
	@RequestMapping("/searchorder.do")
	@ResponseBody
	public Result searchOrderInfo(String type, String keywords) {
		System.out.println("输入关键字进行搜索");
		Result rs=os.searchOrderInfo(type, keywords);
		System.out.println(rs);
		return rs;
	}
}
