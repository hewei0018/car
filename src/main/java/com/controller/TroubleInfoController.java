package com.controller;

import com.service.TroubleInfoService;
import com.util.Result;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * 故障信息管理
 * 故障信息为预先设定好的，在维修订单中只需添加故障码
 */
@Controller
@RequestMapping("/troubleinfo")
public class TroubleInfoController {
	@Resource
	private TroubleInfoService ts;

	//添加故障信息
	@RequestMapping("/add.do")
	@ResponseBody
	public Result addTroubleInfo(String trouble_code, String trouble_name, String trouble_remark) {
		System.out.println("添加故障信息");
		Result rs= ts.addTroubleInfo(trouble_code, trouble_name, trouble_remark);
		System.out.println(rs.toString());
		return rs;
	}
	
	//查询所有故障信息，并返回
	@RequestMapping("/show.do")
	@ResponseBody
	public Result troubleList() {
		System.out.println("展示所有的故障信息");
		Result rs=ts.troubleList();
		System.out.println(rs.toString());
		return rs;
	}

	//根据传入的id，选择启用或者禁用故障
	@RequestMapping("/change.do")
	@ResponseBody
	public Result changStatus(String id) {
		System.out.println("进入TroubleInfoController/change.do");
		Result rs=ts.changStatus(id);
		System.out.println(rs.toString());
		return rs;
	}
}
