package com.controller;

import com.service.PartsInfoService;
import com.util.Result;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * 零件管理
 * 零件购买和使用的功能
 */
@Controller
@RequestMapping("/partsinfo")
public class PartsInfoController {
	@Resource
	private PartsInfoService ps;

	//传入零件的id，然后此零件加1
	@RequestMapping("/buy.do")
	@ResponseBody
	public Result buyParts(String id) {
		System.out.println("购买零件，根据id增加1");
		Result rs=ps.buyParts(id);
		System.out.println(rs);
		return rs;		
	}

	//使用零件，零件减1
	@RequestMapping("/use.do")
	@ResponseBody
	public Result useParts(String id) {
		System.out.println("使用零件，根据id减少1");
		Result rs=ps.useParts(id);
		System.out.println(rs);
		return rs;		
	}

	//返回所有零件的列表
	@RequestMapping("/show.do")
	@ResponseBody
	public Result findAllParts() {
		System.out.println("查询所有零件");
		Result rs=ps.findAllParts();
		System.out.println(rs);
		return rs;
	}
}
