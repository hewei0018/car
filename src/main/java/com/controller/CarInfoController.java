package com.controller;

import com.service.CarInfoService;
import com.util.Result;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * 车辆信息管理
 * 用于后台和用户界面的车辆信息管理功能
 */
@Controller
@RequestMapping("/carinfo")
public class CarInfoController {
	@Resource
	private CarInfoService cs;

	//用户界面，添加车辆信息中传来车辆信息，调用dao存入
	@RequestMapping("/add.do")
	@ResponseBody
	public Result execute(String user_id,
			String plate,
			String brand,
			String model,
			String color,
			String price,
			String date,
			String remark){
		System.out.println("添加车辆信息：");
		Result rs=cs.addCarInfo(user_id, plate, brand, model, color, price, date, remark);
		System.out.println(rs.toString());
		return rs;
	}
	
	//用户界面，查看车辆信息，根据用户id查看
	@RequestMapping("/show.do")
	@ResponseBody
	public Result show(String user_id){
		System.out.println("user_id="+user_id);
		Result rs=cs.showCarInfo(user_id);
		System.out.println(rs.toString());
		return rs;
	}

    //查看车辆信息，根据id查看
	@RequestMapping("/showone.do")
	@ResponseBody
	public Result showOneCar(String id){
		System.out.println("id="+id);
		Result rs=cs.showOneCar(id);
		System.out.println(rs.toString());
		return rs;
	}

	//查看所有的车辆信息
	@RequestMapping("/showAll.do")
	@ResponseBody
	public Result showAllCar() {
		Result rs=cs.showAllCar();
		System.out.println(rs.toString());
		return rs;
	}

    //根据传入的类型和值，模糊查询，在业务层进行对信息的判断
	@RequestMapping("/search.do")
	@ResponseBody
	public Result searchCar(String type,String keywords){
		System.out.println("通过"+type+"和"+keywords+"查询");
		Result rs=cs.searchCar(type, keywords);
		return rs;
	}

	//根据传入的id信息，删除车辆信息
	@RequestMapping("/delete.do")
	@ResponseBody
	public Result delete(String id){
		System.out.println("根据id删除车辆信息！");
		Result rs=cs.delCarInfo(id);
		System.out.println(rs.toString());
		return rs;
	}

	//根据传入的参数，更新车辆信息
	@RequestMapping("/update.do")
	@ResponseBody
	public Result update(String id, String plate, String brand, String model, String color, String price,
			String date, String remark) {
		System.out.println("更新车辆信息！");
		Result rs=cs.updateCarInfo(id, plate, brand, model, color, price, date, remark);
		System.out.println(rs.toString());
		return rs;
	}
}
