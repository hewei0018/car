package com.service;

import com.dao.CarInfoDao;
import com.dao.UserDao;
import com.entity.CarInfo;
import com.entity.User;
import com.util.IDUtil;
import com.util.Result;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CarInfoServiceImpl implements CarInfoService{

	@Resource
	CarInfoDao dao;
	
	@Resource
	UserDao ud;

	//添加车辆信息到数据库
	@Override
	public Result addCarInfo(String user_id, String plate, String brand, String model, String color, String price,
			String date, String remark) {
        //先对参数进行判空
		if(plate.equals("")){
			return new Result("1", "请输入车牌", null);
		}
		//创建车辆对象
		CarInfo ci=new CarInfo();
		//汽车的id通过id工具类自动生成
		ci.setId(IDUtil.createId());
		ci.setUser_id(user_id);
		//创建user对象，并根据传入的用户id查询用户名字
		User user=ud.findUser(user_id);
		ci.setUser_name(user.getName());
		ci.setPlate(plate);
		ci.setBrand(brand);
		ci.setModel(model);
		ci.setPrice(price);
		ci.setDate(date);
		ci.setColor(color);
		ci.setRemark(remark);
		dao.addCarInfo(ci);
		
		return new Result("0", "车辆信息添加成功", null);
	}

	@Override
	public Result showCarInfo(String user_id) {

		List<CarInfo> list=dao.showCarInfo(user_id);
		System.out.println(list.size()+"条数据");
		return new Result("0", "车辆信息加载成功", list);
	}

	@Override
	public Result delCarInfo(String id) {

		dao.delCarInfo(id);
		return new Result("0", "车辆信息删除成功", null);
	}

	@Override
	public Result updateCarInfo(String id, String plate, String brand, String model, String color, String price,
			String date, String remark) {
        //先对输入进行判空，再创建对象更新数据
		if(plate.equals("")){
			return new Result("1", "请输入车牌", null);
		}
		CarInfo ci=new CarInfo();
		ci.setId(id);
		ci.setPlate(plate);
		ci.setBrand(brand);
		ci.setDate(date);
		ci.setColor(color);
		ci.setModel(model);
		ci.setPrice(price);
		ci.setRemark(remark);
		dao.updateCarInfo(ci);
		return new Result("0", "车辆信息修改成功", null);
	}

	@Override
	public Result showOneCar(String id) {

		CarInfo ci=dao.showOneCar(id);
		return new Result("0", "车辆信息加载成功", ci);
	}

	@Override
	public Result showAllCar() {

		List<CarInfo> list =dao.showAllCar();
		return new Result("0", "车辆信息加载成功", list);
	}

	@Override
	public Result searchCar(String type,String keywords) {
        //由于传入的值在后期需要拼接，使用map集合来存储数据
		Map<String,Object> params=new HashMap<String,Object>();
        //先对参数进行判空，不为空再进行
		if(type.equals("")){
			return new Result("1", "请选择分类", null);
		}else if(keywords.equals("")){
			return new Result("1", "请输入关键字", null);
		//类型为车牌 模糊搜索关键字
		}else if(type.equals("1")){
			params.put("plate", "%"+keywords+"%");
		//类型为品牌 模糊搜索关键字
		}else if(type.equals("2")){
			params.put("brand", "%"+keywords+"%");
		//类型为用户 模糊搜索关键字
		}else if(type.equals("3")){
			params.put("user_name", "%"+keywords+"%");
		}
		List<CarInfo> list=dao.searchCarInfo(params);
		return new Result("0", "车辆信息加载成功", list);
	}

}
