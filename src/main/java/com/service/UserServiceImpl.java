package com.service;

import com.dao.PersonalInfoDao;
import com.dao.UserDao;
import com.dao.VisitorDao;
import com.entity.User;
import com.entity.Visitor;
import com.util.IDUtil;
import com.util.MD5Util;
import com.util.Result;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class UserServiceImpl implements UserService{
	
	@Resource
	private UserDao dao;
	
	@Resource
	private VisitorDao vd;
	
	@Resource
	private PersonalInfoDao pidao;
	
	public Result login(String userName, String userPwd) {
		System.out.println("login业务层运行");
		//先从数据库中取出user数据
		User user=dao.userLogin(userName);
		System.out.println(user.toString());
		Result nr=null;
		//调用解密方法对密码进行解密，将解密后的结果存入
		String md5Password=MD5Util.md5(userPwd);
		//对登录信息进行判断
		if(user==null){
			nr=new Result("1", "用户名错误", null);
		}else if(!user.getPassword().equals(md5Password)){
			nr=new Result("1", "密码错误", null);
		}else if(user.getStatus()==1){
			nr=new Result("1", "该用户已被禁用", null);
		//判断通过，存储数据并返回
		}else{
			Integer type=user.getType();
			String userId=user.getId();
			Map<String,Object> data=new HashMap<String, Object>();
			data.put("type", type);
			data.put("userId", userId);
            //将用户身份id返回
			nr=new Result("0", "登陆成功", data);
			//添加访问记录，如果是管理员用户就记录下来
			if(user.getType()==0){	
				List<Visitor> v_list=vd.visitorList();
				if(v_list.size()>=12){
					vd.delVisitor();
				}
				Visitor v=new Visitor();
				v.setId(IDUtil.createId());
				v.setV_name(userName);
				v.setV_time(getTime());
				v.setLong_time(System.currentTimeMillis());
				vd.addVisitor(v);
			}
		}
		return nr;	
	}
	
	private static String getTime(){

		SimpleDateFormat formatter=new SimpleDateFormat("yyyy年MM月dd日   HH:mm:ss");       
		Date curDate=new Date(System.currentTimeMillis());//获取当前时间       
		String str=formatter.format(curDate); 
		return str;
	}
	

	public Result changePwd(String user_id, String mpass, String newpass) {
	    //根据id查询到用户对象
		User user=dao.findUser(user_id);
		Result nr=null;
		//进行判断不为空后进入下一层判断
		if(mpass.equals("")){
			nr=new Result("1", "请输入原始密码", null);
		}else if(newpass.equals("")){
			nr=new Result("1", "请输入新密码", null);
		}else if(mpass.equals("") && newpass.equals("")){
			nr=new Result("1", "请输入密码", null);
		}else if(!MD5Util.md5(mpass).equals(user.getPassword())){
			nr=new Result("1", "原始密码不正确", null);
		}else{
			dao.changPwd(user_id, MD5Util.md5(newpass));
			nr=new Result("0", "密码修改成功", null);
		}
		return nr;		
	}

	@Override
	public Result userList() {

		List<User> list=dao.userList();
		for (User u: list) {
			System.out.println(u.getName());
		}
		return new Result("0", "用户信息加载成功", list);
	}

	@Override
	public Result addUser(String name, String password, Integer type) {
	    //添加用户，为了避免姓名重复，先取出所有信息
		List<User> list=dao.userList();		
		Result nr=null;
		//进行判断，不为空再继续下面
		if(name.equals("")){
			nr=new Result("1", "请输入用户名", null);
		}else if(password.equals("")){
			nr=new Result("1", "请输入密码", null);
		//为了避免用户名重复，进行遍历判断
		}else{
			for(int i=0;i<list.size();i++){
				if(list.get(i).getName().equals(name)){
					nr=new Result("1", "该用户名已存在", null);
					return nr;
				}
			}
			//通过判断，开始添加信息
			User user=new User();
			String id=IDUtil.createId();
			user.setId(id);
			user.setName(name);
			user.setPassword(MD5Util.md5(password));
			user.setType(type);
			user.setStatus(0);
			dao.addUser(user);
			
			pidao.addUserId(id);
			nr=new Result("0", "用户添加成功", null);
		}			
			return nr;

	}

	@Override
	public Result changeStatus(String id) {
	    //根据传入的id进行查找用户
		User user=dao.findUser(id);
		//获取此用户的状态信息
		Integer status=user.getStatus();
		//如果是禁用，就开启
		if(status==0){
			dao.changStatus(id, 1);
		//如果是开启，就禁用
		}else{
			dao.changStatus(id, 0);
		}
		return new Result("0", "用户状态更改成功", null);
	}
	
}
