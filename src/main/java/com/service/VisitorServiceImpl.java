package com.service;

import com.dao.VisitorDao;
import com.entity.Visitor;
import com.util.Result;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class VisitorServiceImpl implements VisitorService{

	@Resource
	VisitorDao dao;
	
	@Override
	public Result visitorList() {

		List<Visitor> list=dao.visitorList();
		return new Result("0", "访客信息加载成功", list);
	}

}
