package com.service;

import com.dao.TroubleInfoDao;
import com.entity.TroubleInfo;
import com.util.IDUtil;
import com.util.Result;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class TroubleInfoServiceImpl implements TroubleInfoService{

	@Resource
	TroubleInfoDao dao;
	
	@Override
	public Result addTroubleInfo(String trouble_code, String trouble_name, String trouble_remark) {
		//添加故障的信息，先判空
		Result nr=null;
		if(trouble_code.equals("")){
			nr=new Result("1", "请输入故障码", null);
		}else if(trouble_name.equals("")){
			nr=new Result("1", "请输入故障名称", null);
		}else{
			//调用id类获取id
			String id=IDUtil.createId();
			TroubleInfo ti=new TroubleInfo();
			ti.setId(id);
			ti.setTrouble_code(trouble_code);
			ti.setTrouble_name(trouble_name);
			ti.setTrouble_remark(trouble_remark);
			ti.setStatus(0);
			dao.addTroubleInfo(ti);
			nr=new Result("0", "故障信息添加成功", null);
		}		
		return nr;
	}

	@Override
	public Result troubleList() {

		List<TroubleInfo> list=dao.troubleList();
		return new Result("0", "故障信息加载成功", list);
	}

	@Override
	public Result changStatus(String id) {
		//先通过id查询出此状态
		TroubleInfo ti=dao.findTrouble(id);
		Integer status=ti.getStatus();
		//禁用就开启
		if(status==0){
			dao.changStatus(id, 1);
		//开启就禁用
		}else{
			dao.changStatus(id, 0);
		}
		return new Result("0", "故障状态更改成功", null);
	}

}
