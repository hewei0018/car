package com.util;

/**
 * 返回结果的工具类 用于将系统的一些信息封装 然后返回到页面上
 *  方便页面弹出信息
 */
public class Result {
    //状态信息 1为异常，0为正常
	private String status;
    //操作的信息
	private String msg;
    //数据，使用ResponseBody注解 将数据返回
	private Object data;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Object getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public Result(String status, String msg, Object data) {
		super();
		this.status = status;
		this.msg = msg;
		this.data = data;
	}
	@Override
	public String toString() {
		return "NoteResult [status=" + status + ", msg=" + msg + ", data=" + data + "]";
	}
	
}
