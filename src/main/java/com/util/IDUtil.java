package com.util;

import java.util.Calendar;

/**
 * 生成一个唯一的数字字符串
 * 作为车辆的id和用户的id
 * 和零件的id区分开
 */
public class IDUtil {

	/**
	 * 生成一个令牌号
	 */
	public static String createToken(){
		String str =createId();
		return str.replaceAll("-", "");
	}
	
	/**
	 * 采用UUID算法生成一个唯一性的字符串
	 */
	public static String createId(){
//		UUID uuid=UUID.randomUUID();
//		String id=uuid.toString();
		Calendar calendar = Calendar.getInstance();
		String id=String.valueOf(calendar.getTime().getTime());
		return id;
	}
	
	public static void main(String[] args) {
		System.out.println(createId());
	}
}
